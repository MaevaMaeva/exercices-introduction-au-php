<!DOCTYPE html>
<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Fonctions PHP</title>
</head>
<body>
<p>
  <h1>Les fonctions</h1>  
  <h2>Exercice 1 et 2</h2>

  <?php
// Faire une fonction qui retourne true.
afficher();

function afficher (){
    return true;
}
// Faire une fonction qui prend en paramètre une chaine de caractères 
// et qui retourne cette même chaine.
$hi="";
function bonjour ($hi){
    return $hi;
}


echo bonjour("hello");




   ?>
   <h2>Exercice 3</h2>
   <?php
//    Faire une fonction qui prend en paramètre deux chaines de caractères 
//    et qui renvoit la concaténation de ces deux chaines.
$cdc1 = "";
$cdc2 = "";
function maFonction ($cdc1, $cdc2) {
    return $cdc1.$cdc2;
}

echo maFonction("hi"," !");
    ?>
    <h2>Exercice 4</h2>
    <?php
    // Faire une fonction qui prend en paramètre deux nombres. La fonction doit retourner :
    // Le premier nombre est plus grand si le premier nombre est plus grand que le deuxième
    // Le premier nombre est plus petit si le premier nombre est plus petit que le deuxième
    // Les deux nombres sont identiques si les deux nombres sont égaux
$nbr1 = 2;
$nbr2 = 1;
function compare ($nbr1,$nbr2){
    if ($nbr1>$nbr2){
        return "le premier nombre est plus grand que le second";
        
    }
    elseif ($nbr1<$nbr2){
        return "le premier nombre est plus petit que le second";
    }
    elseif ($nbr1===$nbr2) {
        return "les deux nombres son identiques";
    }
}
echo compare(3,5);
echo compare(4,4);
echo compare (10,3);
     ?>
     <h1>Exercice 5</h1>
     <?php
    //  Faire une fonction qui prend trois paramètres : 
    //  nom, prenom et age. 
    //  Elle doit renvoyer une chaine de la forme :
    //  "Bonjour" + nom + prenom + ", tu as " + age + "ans".
 
    function saluer($nom, $prenom, $age){
        $slt = "Bonjour " . $nom . " " . $prenom . ", tu as " . $age . " ans.";
        return $slt;
    }
    echo saluer("Morceau", "Maëva", 31);

     ?>
     <h1>Exercice 6</h1>
     <?php
//     Faire une fonction qui prend deux paramètres : age et genre. 
//     Le paramètre genre peut prendre comme valeur Homme ou Femme. 
//La fonction doit renvoyer en fonction des paramètres (gérer tous les cas) :
// Vous êtes un homme et vous êtes majeur
// Vous êtes un homme et vous êtes mineur
// Vous êtes une femme et vous êtes majeur
// Vous êtes une femme et vous êtes mineur
    function genree ( $age, $genre){
       if ($genre === "Femme") {
           $houf = "Vous êtes une femme et ";
       }else {
           $houf = "Vous êtes un homme et ";
       }
            if ($age >= 18){
            $majorite = "vous êtes majeur";
            } else {
            $majorite = "vous êtes mineur";
        };
        return $houf . $majorite;
        
    }
    echo genree(25, "Homme");
    echo genree(32, "Femme");
    echo genree(14, "Homme");
    echo genree(9, "Femme");
     ?>
       <h1>Exercice 7</h1>
     <?php
    //  Faire une fonction qui prend en paramètre trois nombres et 
    //  qui renvoit la somme de ces nombres. 
    //  Tous les paramètres doivent avoir une valeur par défaut.
     $nbr_1=1;
     $nbr_2=2;
     $nbr_3=3;

    function somme($nbr_1,$nbr_2,$nbr_3){
        $sum = $nbr_1+$nbr_2+$nbr_3;
        return $sum;
     };
     echo somme(2,4,34);
     
     ?>
   </p>
</body>
</html>