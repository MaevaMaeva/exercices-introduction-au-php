<!DOCTYPE html>
<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tableaux PHP</title>
</head>
<body>
<div>
<h1>Tableaux</h1>

<?php
//Pour mettre des titres :
function titre ($title){
    echo '<h2>EXERCICE '.$title.'</h2>';
}

// Faire en sorte grâce à une fonction que ce soit facile et joli d'afficher
function afficher ($aff){
    echo '<p>'.$aff .'</p>';
}

titre ('1');
// Exercice 1 Créer un tableau $mois et l'initialiser avec le nom des douze mois de l'année.
$mois =[
    'Janvier',
    'Février',
    'Mars',
    'Avril',
    'Mai',
    'Juin',
    'Juillet',
    'Aout',
    'Septembre',
    'Octobre',
    'Novembre',
    'Décembre',
];


titre ('2');
// Exercice 2 Avec le tableau de l'exercice 1, afficher la valeur de la troisième ligne de ce tableau.(Mars)
echo $mois[2]; // ou afficher ($mois[2];)

titre ('3');
// Exercice 3 Avec le tableau de l'exercice , afficher la valeur de l'index 5.
afficher ($mois[5]);

titre ('4');
// Exercice 4 Avec le tableau de l'exercice 1, modifier le mois de aout pour lui ajouter l'accent manquant.
$mois[7] = 'Août';

titre ('5');
// Exercice 5 Créer un tableau avec comme index le bon numéro des mois en valeur leur nom.
$mois2 =[
   1 => 'Janvier',
   2 =>  'Février',
   3 => 'Mars',
   4 => 'Avril',
   5 =>  'Mai',
   6 =>  'Juin',
   7 =>  'Juillet',
   8 =>  'Aout',
   9 => 'Septembre',
   10 =>  'Octobre',
   11 => 'Novembre',
   12 =>  'Décembre',
];

titre ('6');
// Exercice 6 Avec le tableau de l'exercice 5, afficher la valeur de l'index 11.
afficher ($mois2[11]);

titre ('7');
// Exercice 7 Avec le tableau de l'exercice 5, ajouter la ligne d'un 13eme mois.
$mois2[13] = 'Treizième';

titre ('8');
// Exercice 8 Avec le tableau de l'exercice 1 et une boucle, afficher toutes les valeurs de ce tableau.
for ($i = 1; $i<= count($mois2); $i++) {
    afficher ($mois2[$i]);
}

titre ('9');
// Exercice 9 Avec le tableau de l'exercice 5, afficher toutes les valeurs de ce tableau.
// foreach ($mois as $m){
//     afficher ($m);
// }
//OU :
foreach ($mois as $index => $value){
    afficher ($index);
    afficher ($value);
}
titre ('10');
// // Exercice 10 Avec le tableau de l'exercice 5, 
// afficher toutes les valeurs de ce tableau ainsi que les clés associés. 
// Cela pourra être, par exemple, de la forme : 
//     "Le département" + nom_departement + "a le numéro" + num_departement
// // 

?>
    </div>
</body>
</html>