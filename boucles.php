<!DOCTYPE html>
<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Boucles PHP</title>
</head>
<body>
<h1>I Les boucles</h1>
<h2>Exercice 1</h2>
<?php 
// Exercice 1 Créer une variable et l'initialiser à 0. Tant que cette variable n'atteint pas 10 :
// l'afficher
// incrémenter de 1


$premiere = 0;
while ($premiere < 10) {
echo $premiere."</br>";
$premiere++; 
}
?>
<h2>Exercice 2</h2>
<?php 
// Exercice 2 Créer deux variables. Initialiser la première à 0 
// et la deuxième avec un nombre compris en 1 et 100. 
// Tant que la première variable n'est pas supérieur à 20 :
//     multiplier la première variable avec la deuxième
//     afficher le résultat
//     incrémenter la première variable

$a = 0;
$b = 50;
while ($a <= 20){
    $ab = $a * $b;
    echo $ab."</br>";
    $a++;
}

?>
<h2>Exercice 3</h2>
<?php
// Exercice 3 Créer deux variables. 
// Initialiser la première à 100 et la deuxième avec un nombre compris en 1 et 100. 
// Tant que la première variable n'est pas inférieur ou égale à 10 :
// multiplier la première variable avec la deuxième
// afficher le résultat
// décrémenter la première variable
$c = 100;
$d = 30;
while (!($c <= 10)){ //ou ($c >=10) reviendrait au même
   $cd = $c * $d ;
    echo $cd."</br>";
    $c--;
}

?>
<h2>Exercice 4</h2>
<?php
// Exercice 4 Créer une variable et l'initialiser à 1. 
// Tant que cette variable n'atteint pas 10 :
// l'afficher
// l'incrementer de la moitié de sa valeur

$e =1;
while ($e <=10){
    echo $e."</br>";
    $e += $e/2;
}

 ?>
 <h2>Exercice 5</h2>
 <?php
//  En allant de 1 à 15 avec un pas de 1, afficher le message On y arrive presque...

$f = 1;
while ($f <=15){
    echo $f."...On y arrive presque...</br>";
    $f++;
}
 ?>
 <h2>Exercice 6</h2>
 <?php
// En allant de 20 à 0 avec un pas de 1, afficher le message C'est presque bon...
$g = 20;
while (!($g===-1)){
    echo $g."...C'est presque bon...</br>";
    $g--;
}
 ?>
 <h2>Exercice 7</h2>
 <?php 
//  En allant de 1 à 100 avec un pas de 15, afficher le message On tient le bon bout...
$h = 1;
while ($h <= 100){
    echo $h."...On tient le bon bout...</br>";
    $h += 15;
}
?>
<h2>Exercice 8</h2>
<?php 
// En allant de 200 à 0 avec un pas de 12, afficher le message Enfin ! ! !
$i = 200;
while ($i>0){
    echo $i."...Enfin !!!</br>";
    $i-=12;
}
?>
</p>
    
</body>
</html>